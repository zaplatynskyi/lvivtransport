package com.example.dop.navdrawer;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.example.dop.navdrawer.fragments.FragmentGallery;
import com.example.dop.navdrawer.fragments.FragmentImport;
import com.example.dop.navdrawer.fragments.FragmentSend;
import com.example.dop.navdrawer.fragments.FragmentShare;
import com.example.dop.navdrawer.fragments.FragmentSlideShow;
import com.example.dop.navdrawer.fragments.FragmentTools;
import com.example.dop.navdrawer.model.ListAdapter;
import com.example.dop.navdrawer.model.ListAdapter.customButtonListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import static android.app.PendingIntent.getActivity;
import static android.widget.ArrayAdapter.createFromResource;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, ViewSwitcher.ViewFactory,customButtonListener {

    FragmentGallery fGallery;
    FragmentImport fImport;
    FragmentSend fSend;
    FragmentShare fShare;
    FragmentSlideShow fSlideShow;
    FragmentTools fTools;

    private ListView listViewButton;
    ListAdapter adapterButton;
    ArrayList<String> dataItems = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
//                {
//                    public void onDrawerClosed(View view) {
//                        getActionBar().setTitle("Закрито");
//                        invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
//                    }
//
//                    public void onDrawerOpened(View drawerView) {
//                        getActionBar().setTitle("Відкрито");
//                        invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
//                    }
//                };
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        // ImageSwitcher init
        final ImageSwitcher imageSwitcher  = (ImageSwitcher) findViewById(R.id.imageSwitcher);
        imageSwitcher.setFactory(this);

        Animation inAnim = new AlphaAnimation(0, 1);
        inAnim.setDuration(500);
        Animation outAnim = new AlphaAnimation(1, 0);
        outAnim.setDuration(700);

        imageSwitcher.setInAnimation(inAnim);
        imageSwitcher.setOutAnimation(outAnim);

        imageSwitcher.setImageResource(R.drawable.tram);

        // получаем экземпляр элемента ListView
       // ListView listView = (ListView) findViewById(R.id.list_view_inside_nav);
        // определяем массив типа String
        final String[] catnames = new String[]{
                "Рыжик", "Барсик", "Мурзик", "Мурка", "Васька",
                "Томасина", "Кристина", "Пушок", "Дымка", "Кузя",
                "Китти", "Масяня", "Симба"
        };
        /*
        // используем адаптер данных
        final ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, catnames);
        listView.setAdapter(adapter1);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getApplicationContext(), "Item clicked", Toast.LENGTH_SHORT).show();
            }
        });*/
        String[] dataArray = new String[]{
                "Рыжик", "Барсик", "Мурзик", "Мурка", "Васька",
                "Томасина", "Кристина", "Пушок", "Дымка", "Кузя",
                "Китти", "Масяня", "Симба"
        };
        List<String> dataTemp = Arrays.asList(dataArray);
        dataItems.addAll(dataTemp);
        listViewButton = (ListView) findViewById(R.id.list_view_inside_nav);
        adapterButton  = new ListAdapter(MainActivity.this, dataItems);
        adapterButton.setCustomButtonListner((ListAdapter.customButtonListener) this);
        listViewButton.setAdapter(adapterButton);
        listViewButton.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getApplicationContext(), "Item clicked", Toast.LENGTH_SHORT).show();
            }
        });

        // Initialize spinner
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        final View headerLayout = navigationView.getHeaderView(0);
        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        //   spinner.setBackgroundColor(getResources().getColor(R.color.colorAccent));

        ArrayAdapter<CharSequence> adapter = createFromResource(spinner.getContext(), R.array.transport_type_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Вызываем адаптер
        spinner.setAdapter(adapter);

        //final ImageView icon = (ImageView) findViewById(R.id.imageView);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent,
                                       View itemSelected, int selectedItemPosition, long selectedId) {

                //String[] choose = getResources().getStringArray(R.array.transport);
                switch (selectedItemPosition) {
                    case 0:
                        //icon.setImageResource(R.drawable.tram);
                        imageSwitcher.setImageResource(R.drawable.tram);
                        break;
                    case 1:
                        //icon.setImageResource(R.drawable.bus);
                        imageSwitcher.setImageResource(R.drawable.bus);
                        break;
                }
            }

            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        final EditText editSearchRoutes = (EditText) findViewById(R.id.editSearchRoutes);
        final Button btnClear = (Button) findViewById(R.id.btnClear);
        //set on text change listener for edittext
        editSearchRoutes.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!editSearchRoutes.getText().toString().equals("")) { //if edittext include text
                    btnClear.setVisibility(View.VISIBLE);
                } else { //not include text
                    btnClear.setVisibility(View.GONE);
                    //Toast.makeText(MainActivity.this, "All texts have gone!!!", Toast.LENGTH_SHORT).show();
                }
                String text = editSearchRoutes.getText().toString().toLowerCase(Locale.getDefault());
                adapterButton.getFilter().filter(text);
            }
        });
        //set event for clear button
        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editSearchRoutes.setText("");
                btnClear.setVisibility(View.GONE);
            }
        });

        // Initialize all fragments.
        fGallery = new FragmentGallery();
        fImport = new FragmentImport();
        fSend = new FragmentSend();
        fShare = new FragmentShare();
        fSlideShow = new FragmentSlideShow();
        fTools = new FragmentTools();
    }



    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }

    }


    @Override
    public void onButtonClickListner(int position, String value) {
        adapterButton.getItem(position);
        final ImageView icon = (ImageView) findViewById(R.id.icon);
        icon.setImageResource(R.drawable.star_yellow);

        Toast.makeText(MainActivity.this, "Додано до улюблених " + value,
                Toast.LENGTH_SHORT).show();
    }


    //HIde virtual keyboard when touch out side edittext in Android
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (getCurrentFocus() != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        //getMenuInflater().inflate();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

        if (id == R.id.nav_camera) {
            // Handle the camera action
            fragmentTransaction.replace(R.id.containter, fImport);
        } else if (id == R.id.nav_gallery) {
            fragmentTransaction.replace(R.id.containter, fGallery);
        } else if (id == R.id.nav_slideshow) {
            fragmentTransaction.replace(R.id.containter, fSlideShow);
        } else if (id == R.id.nav_manage) {
            fragmentTransaction.replace(R.id.containter, fTools);
        } else if (id == R.id.nav_share) {
            fragmentTransaction.replace(R.id.containter, fShare);

        } else if (id == R.id.nav_send) {
            fragmentTransaction.replace(R.id.containter, fSend);
        }

        fragmentTransaction.commit();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        setTitle(item.getTitle());
        return true;
    }

    @Override
    public View makeView() {
        ImageView imgview = new ImageView(this);
        imgview.setScaleType(ImageView.ScaleType.FIT_CENTER);
        imgview.setLayoutParams(new
                ImageSwitcher.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
       // imgview.setBackgroundColor(0xFF000000);
        return imgview;
    }
}
